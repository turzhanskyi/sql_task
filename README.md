# SQL tasks

SQL-вибірки, для пошуку за заданими параметрами:

* SQL task Given tables:
* 01​ . tasks (id, name, status, project_id)
* 02.​ projects (id, name)

####Technical requirements


#### 1) get all statuses, not repeating, alphabetically ordered 
* SELECT DISTINCT status FROM tasks ORDER BY status;

#### 2) get the count of all tasks in each project, order by tasks count descending
* SELECT projects.id, COUNT(tasks.id) AS count FROM projects LEFT JOIN tasks ON projects.id = tasks.project_id GROUP BY projects.id ORDER BY count DESC;

#### 3) get the count of all tasks in each project, order by projects names
* SELECT name, (SELECT COUNT(*) FROM tasks WHERE project_id = projects.id) FROM projects ORDER BY name;

#### 4) get the tasks for all projects having the name beginning with  "N" letter
* SELECT * FROM tasks WHERE name like 'N%';

#### 5) get the list of all projects containing the 'a' letter in the middle of the name, and show the tasks count near each project. Mention that there can exist projects without tasks and tasks with project_id = NULL
* SELECT projects.name, COUNT(tasks.id) FROM projects RIGHT JOIN tasks ON projects.id = project_id WHERE projects.name LIKE '%є%' GROUP BY projects.name;

#### 6) get the list of tasks with duplicate names. Order alphabetically
* SELECT * FROM tasks WHERE name IN (SELECT name FROM tasks GROUP BY name HAVING COUNT(name)>1 ) ORDER BY name ASC;

#### 7) get list of tasks having several exact matches of both name and status, from the project 'Garage'. Order by matches count
* SELECT tasks.name FROM tasks RIGHT JOIN projects ON tasks.projects_id = projects.id WHERE projects.name = 'Garage' GROUP BY tasks.name, tasks.status HAVING COUNT(tasks) >1 ORDER BY COUNT(tasks);

#### 8) get the list of project names having more than 10 tasks in status 'completed'. Order by project_id
* SELECT name FROM projects WHERE (SELECT COUNT(*) FROM tasks WHERE tasks.project_id = projects.id AND tasks.status = 'completed') > 10 ORDER BY id;








