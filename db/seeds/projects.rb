if Project.count.zero?
  puts 'Seeding Projects'
  Project.create!(name: 'Garage')
  Project.create!(name: 'Проєкт №2')
  Project.create!(name: 'Проєкт №1')
  Project.create!(name: 'For Home')
  Project.create!(name: 'Complete the test task')
end
