if Task.count.zero?
  puts 'Seeding Tasks'
  Task.create!(name: 'Завдання №1', status: 'статус 1', project_id: 1)
  Task.create!(name: 'Завдання №2', status: 'статус 1', project_id: 1)
  Task.create!(name: 'Нове Завдання №3', status: 'статус 3', project_id: 2)
  Task.create!(name: 'Завдання', status: 'статус 4', project_id: 3)
  Task.create!(name: 'Старе Завдання №5', status: 'статус 5', project_id: 5)
  Task.create!(name: 'Завдання', status: 'статус 6', project_id: 1)
  Task.create!(name: 'Завдання №7', status: 'статус 7', project_id: 2)
end